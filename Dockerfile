FROM ubuntu:20.04

ENV DEBIAN_FRONTENV noninteractive

# Fix timezone issue
ENV TZ=Europe/London
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

ARG user_id
ARG group_id

# Required Packages for:
# 1. the Host Development System
# 2. Freescale compilation
# 3. Additional host packages required by poky/scripts/wic
# 4. Development edition
# http://www.yoctoproject.org/docs/latest/mega-manual/mega-manual.html#required-packages-for-the-host-development-system

# Environment configuration:
# 1. Add "repo" tool (used by many Yocto-based projects)
# 2. Create a non-root user that will perform the actual build
# 3. Fix error "Please use a locale setting which supports utf-8."
#    See https://wiki.yoctoproject.org/wiki/TipsAndTricks/ResolvingLocaleIssues
# 4. clean useless packages, remove apt itself, man, locales and doc files.

RUN apt-get update && apt-get -y upgrade && \
    apt-get install -y --no-install-recommends \
        bc \
        bison \
        build-essential \
        bzip2 \
        ca-certificates \
        chrpath \
        cpio \
        debianutils \
        diffstat \
        expect \
        file \
        flex \
        gawk \
        git-lfs \
	icecc \
        kmod \
        liblz4-tool \
        libssl-dev \
        locales \
        lzop \
        mtd-utils \
        openssh-client \
        python2 \
        python3-distutils \
        python-is-python3 \
        rsync \
        sudo \
        texinfo \
        time \
        unzip \
        u-boot-tools \
        wget \
        xz-utils \
        zstd \
    && if [ "$(uname -m)" = "x86_64" ]; then \
        apt-get install -y \
            g++-7-multilib \
            g++-8-multilib \
            g++-9-multilib \
            g++-10-multilib \
            g++-multilib \
        && apt-get clean && apt-get autoclean; \
    else \
        apt-get install -y \
            g++-7 \
            g++-8 \
            g++-9 \
            g++-10 \
            g++ \
        && apt-get clean && apt-get autoclean; \
    fi \
    && update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-10 100 --slave /usr/bin/g++ g++ /usr/bin/g++-10 --slave /usr/bin/gcov gcov /usr/bin/gcov-10 \
    && update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-9 90 --slave /usr/bin/g++ g++ /usr/bin/g++-9 --slave /usr/bin/gcov gcov /usr/bin/gcov-9 \
    && update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-8 80 --slave /usr/bin/g++ g++ /usr/bin/g++-8 --slave /usr/bin/gcov gcov /usr/bin/gcov-8 \
    && update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-7 70 --slave /usr/bin/g++ g++ /usr/bin/g++-7 --slave /usr/bin/gcov gcov /usr/bin/gcov-7 \
    && update-alternatives --set gcc /usr/bin/gcc-10 \
    && \
    wget -qO- http://storage.googleapis.com/git-repo-downloads/repo > /usr/local/bin/repo && \
        chmod a+x /usr/local/bin/repo \
    && \
    groupadd -f --gid ${group_id} build && \
    id build 2>/dev/null || useradd --uid ${user_id} --gid ${group_id} --create-home build && \
        echo "build ALL=(ALL) NOPASSWD: ALL" | tee -a /etc/sudoers \
    && \
    sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen && \
        echo 'LANG="en_US.UTF-8"'>/etc/default/locale && \
        dpkg-reconfigure --frontend=noninteractive locales && \
        update-locale LANG=en_US.UTF-8 \
    && \
    apt-get clean && apt-get autoclean && \
        apt-get autoremove -y --allow-remove-essential apt && \
        rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* && \
        rm -rf /var/cache/apt/archives && \
        rm -rf /usr/share/doc/ /usr/share/man/ /usr/share/locale \
    && \
    echo "Finish"

# Enable high priority builds
RUN echo "build - nice -20" | tee -a /etc/security/limits.conf

ENV LC_ALL en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US.UTF-8

USER build
WORKDIR /home/build
CMD "/bin/bash"

# All builds will be done by user aosp
COPY --chown=build:build copy_files/git_config /home/build/.gitconfig
COPY --chown=build:build copy_files/ssh_config /home/build/.ssh/config
COPY --chown=build:build copy_files/inputrc /home/build/.inputrc

COPY --chown=build:build scripts/nxp_build.sh /home/build/nxp_build.sh

# EOF
