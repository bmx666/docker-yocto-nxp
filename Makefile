DOCKER = docker
ifeq (buildx,$(shell $(DOCKER) buildx build --help >/dev/null && echo 'buildx'))
DOCKER := DOCKER_BUILDKIT=1 $(DOCKER)
endif
IMAGE = nxp/yocto
IMAGE_DEVEL = nxp/yocto-devel
UID = $(shell id -u)
GID = $(shell id -g)

DOCKER_OPT_SQUASH = $(shell $(DOCKER) version | grep -m1 'Experimental:' | grep -m1 'true' >/dev/null && $(DOCKER) build --help | grep -m1 '\-\-squash' >/dev/null && echo '--squash')
DOCKER_OPT_BUILD_ARG = --build-arg user_id=$(UID) --build-arg group_id=$(GID)
DOCKER_OPT_NOCACHE = --no-cache

DOCKER_BUILD=$(DOCKER_OPT_BUILD_ARG) $(DOCKER_OPT_SQUASH)
DOCKER_REBUILD=$(DOCKER_BUILD) $(DOCKER_OPT_NOCACHE)

DOCKER_COMMAND ?= "/bin/bash -c '/home/build/nxp_build.sh'"

MAKEFILE := $(word $(words $(MAKEFILE_LIST)), $(MAKEFILE_LIST))
MAKEFILE_DIR := $(abspath $(dir $(MAKEFILE)))

all: build

clean:
	docker image prune -f

build: $(MAKEFILE_DIR)/Dockerfile
	$(DOCKER) build $(DOCKER_BUILD) -t $(IMAGE) -f $(MAKEFILE_DIR)/Dockerfile $(MAKEFILE_DIR)

rebuild: $(MAKEFILE_DIR)/Dockerfile
	$(DOCKER) build $(DOCKER_REBUILD) -t $(IMAGE) -f $(MAKEFILE_DIR)/Dockerfile $(MAKEFILE_DIR)

run:
	DOCKER_IMAGE=$(IMAGE) DOCKER_COMMAND=$(DOCKER_COMMAND) $(MAKEFILE_DIR)/scripts/docker_run.sh

build_devel: $(MAKEFILE_DIR)/Dockerfile.devel
	$(DOCKER) build $(DOCKER_BUILD) -t $(IMAGE_DEVEL) -f $(MAKEFILE_DIR)/Dockerfile.devel $(MAKEFILE_DIR)

rebuild_devel: $(MAKEFILE_DIR)/Dockerfile.devel
	$(DOCKER) build $(DOCKER_REBUILD) -t $(IMAGE_DEVEL) -f $(MAKEFILE_DIR)/Dockerfile.devel $(MAKEFILE_DIR)

run_devel:
	DOCKER_IMAGE=$(IMAGE_DEVEL) DOCKER_COMMAND=$(DOCKER_COMMAND) $(MAKEFILE_DIR)/scripts/docker_run.sh

.PHONY: all clean run build rebuild build_devel rebuild_devel run_devel
