#!/bin/bash

set -e

cd ~/yocto

mkdir -p nxp-bsp-platform

EXT4FS_FILE=ext4fs.img

truncate -s 100G ${EXT4FS_FILE}

if [ "$(file ${EXT4FS_FILE} | grep -o 'ext4 filesystem')" != "ext4 filesystem" ]
then
        mkfs.ext4 ${EXT4FS_FILE}
fi

e2fsck -f ${EXT4FS_FILE}
resize2fs ${EXT4FS_FILE}

LOOP_DEV=$(losetup -fP --show ${EXT4FS_FILE})
trap "losetup -d ${LOOP_DEV}" EXIT

sudo mount ${LOOP_DEV} nxp-bsp-platform
trap "sudo umount ${LOOP_DEV}; losetup -d ${LOOP_DEV}" EXIT

sudo chown build.build nxp-bsp-platform

bash
