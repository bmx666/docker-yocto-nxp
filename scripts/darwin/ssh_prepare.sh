#!/bin/bash

if [ ! -S "$SSH_AUTH_SOCK" ]
then
	# Kill ssh-agent
	trap "ssh-agent -k" exit
	eval "$(ssh-agent -s)"
	ssh-add
fi

if [ ! -S "$SSH_AUTH_SOCK" ]
then
	echo "ssh-agent auth socket not found"
	exit 1
fi
