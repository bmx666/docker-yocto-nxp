#!/bin/bash

if [ -z "$DOCKER_COMMAND" ]
then
	echo "env DOCKER_COMMAND is not set"
	echo -n "Continue? [y/n/?]: "
	read -n1 CHOOSE_YES_NO
	echo ""

	if [[ "${CHOOSE_YES_NO}" =~ ^(y|Y)$ ]]
	then
		echo "Run docker without command"
	else
		echo "Abort"
		exit 1
	fi
	unset CHOOSE_YES_NO
fi

RETVAL=''

function set_choices() {
	LIST_=${1}
	ENV_NAME=${2}
	NAME=${3}
	echo "env ${ENV_NAME} is not set"
	if [[ "${#LIST_[@]}" -eq 1 ]]
	then
		RETVAL="${LIST_[0]}"
		unset LIST_
		if [ "${RETVAL}" != "custom" ]
		then
			echo "autoset ${ENV_NAME} = '${RETVAL}'"
			return
		fi
	else
		echo "Choose ${NAME}:"
		for i in $(seq 1 ${#LIST_[@]})
		do
			echo "${i}. ${LIST_[$((${i}-1))]}"
		done
		echo -n "Enter ${NAME} (1-${#LIST_[@]}): "
		LIST_COUNT=${#LIST_[@]}
		read -n${#LIST_COUNT} CHOOSE_
		unset LIST_COUNT
		echo ""

		if [[ "${CHOOSE_}" -ge 1 && "${CHOOSE_}" -le ${#LIST_[@]} ]]
		then
			RETVAL="${LIST_[$((${CHOOSE_}-1))]}"
			unset LIST_
			unset CHOOSE_
		else
			unset LIST_
			unset CHOOSE_
			echo "Wrong choise"
			exit 1
		fi
	fi

	if [ "${RETVAL}" == "custom" ]
	then
		echo "env ${ENV_NAME} is 'custom'"
		echo "better export env ${ENV_NAME} for future use"
		echo -n "Set ${NAME}: "
		read CHOOSE_NAME

		# remove leading whitespace characters
		CHOOSE_NAME="${CHOOSE_NAME#"${CHOOSE_NAME%%[![:space:]]*}"}"
		# remove trailing whitespace characters
		CHOOSE_NAME="${CHOOSE_NAME%"${CHOOSE_NAME##*[![:space:]]}"}"

		# check on whitespaces inside
		if [[ "${CHOOSE_NAME}" =~ "[[:space:]]+" ]]
		then
			echo "env ${ENV_NAME} has whitespaces inside"
			exit 1
		fi

		if [ -z "${CHOOSE_NAME}" ]
		then
			echo "env ${ENV_NAME} is empty, set 'custom' by default"
			CHOOSE_NAME="custom"
		fi

		RETVAL="${CHOOSE_NAME}"
		unset CHOOSE_NAME
	fi
}

if [ -z "${BUILD_PRIO}" ]
then
	BUILD_PRIO=""
	echo -n "Choose high/low priority for build or skip [h/l/*]: "
	read -n1 CHOOSE_BUILD_PRIO
	echo ""

	if [[ "${CHOOSE_BUILD_PRIO}" =~ ^(h|H)$ ]]
	then
		echo "Using high priority for build"
		BUILD_PRIO=" ionice -c 2 -n 0 nice -n -20 "
		NICE_PRIO_ARGS="--cap-add=SYS_NICE --ulimit nice=40"
	elif [[ "${CHOOSE_BUILD_PRIO}" =~ ^(l|L)$ ]]
	then
		echo "Using low priority for build"
		BUILD_PRIO=" ionice -c 3 nice -n 19 "
	fi
	unset CHOOSE_BUILD_PRIO
fi

if [ -z "${WORKDIR}" ]
then
	echo "env WORKDIR is not set"
	echo -n "Set working directory: "
	read CHOOSE_WORKDIR
	if [ ! -d "${CHOOSE_WORKDIR}" ]
	then
		echo "Directory '${WORKDIR}' is not exists"
		exit 1
	fi
	WORKDIR="${CHOOSE_WORKDIR}"
	unset CHOOSE_WORKDIR
fi

if [ -z "${DISTRO}" ]
then
	LIST_=(\
		"custom"\
		"fsl-imx-x11"\
		"fsl-imx-fb"\
		"fsl-imx-xwayland"\
		"fsl-imx-wayland"\
	)
	set_choices ${LIST_} "DISTRO" "distro"
	unset LIST_
	DISTRO=${RETVAL}
fi

if [ -z "${TARGET_MANIFEST_URL}" ]
then
	LIST_=(\
		"custom"\
		"https://github.com/nxp-imx/imx-manifest"\
	)
	set_choices ${LIST_} "TARGET_MANIFEST_URL" "target manifest url"
	unset LIST_
	TARGET_MANIFEST_URL=${RETVAL}
fi

if [ -z "${TARGET_MANIFEST_BRANCH}" ]
then
	LIST_=(\
		"custom"\
		"imx-linux-rocko"\
		"imx-linux-sumo"\
		"imx-linux-thud"\
		"imx-linux-warrior"\
		"imx-linux-zeus"\
		"imx-linux-gatesgarth"\
		"imx-linux-hardknott"\
		"imx-linux-honister"\
		"imx-linux-kirkstone"\
		"imx-linux-langdale"\
	)
	set_choices ${LIST_} "TARGET_MANIFEST_BRANCH" "target manifest branch"
	unset LIST_
	TARGET_MANIFEST_BRANCH=${RETVAL}
fi

if [ -z "${TARGET_MANIFEST_NAME}" ]
then
	case "${TARGET_MANIFEST_BRANCH}" in
		"imx-linux-rocko")
			LIST_=(\
				"imx-4.9.88-2.0.0_ga"\
			);;
		"imx-linux-sumo")
			LIST_=(\
				"imx-4.14.78-1.0.0_ga"\
				"imx-4.14.78-1.1.10"\
				"imx-4.14.98-2.0.0_ga"\
				"imx-4.14.98-2.3.3"\
			);;
		"imx-linux-thud")
			LIST_=(\
				"imx-4.19.35-1.0.0"\
			);;
		"imx-linux-warrior")
			LIST_=(\
				"imx-4.19.35-1.1.2"\
			);;
		"imx-linux-zeus")
			LIST_=(\
				"imx-5.4.3-1.0.1"\
				"imx-5.4.3-2.0.0"\
				"imx-5.4.24-2.1.0"\
				"imx-5.4.47-1.0.1"\
				"imx-5.4.47-2.2.0"\
				"imx-5.4.70-2.3.8"\
			);;
		"imx-linux-gatesgarth")
			LIST_=(\
				"imx-5.10.9-1.0.0"\
			);;
		"imx-linux-hardknott")
			LIST_=(\
				"imx-5.10.35-2.0.0"\
				"imx-5.10.52-2.1.0"\
				"imx-5.10.72-2.2.0"\
				"imx-5.10.72-2.2.1"\
				"imx-5.10.72-2.2.2"\
				"imx-5.10.72-2.2.3"\
			);;
		"imx-linux-honister")
			LIST_=(\
				"imx-5.15.5-1.0.0"\
			);;
		"imx-linux-kirkstone")
			LIST_=(\
				"imx-5.15.32-2.0.0"\
				"imx-5.15.52-2.1.0"\
				"imx-5.15.71-2.2.0"\
			);;
		"imx-linux-langdale")
			LIST_=(\
				"imx-6.1.1-1.0.0"\
			);;
		"custom" | *)
			LIST_=("custom");;
	esac
	set_choices ${LIST_} "TARGET_MANIFEST_NAME" "target manifest name"
	unset LIST_
	TARGET_MANIFEST_NAME=${RETVAL}
fi

if [ -z "${MACHINE}" ]
then
	case "${TARGET_MANIFEST_BRANCH}" in
		"imx-linux-hardknott")
			LIST_=(\
				"custom"\
				"imx6qpsabreauto"\
				"imx6qpsabresd"\
				"imx6ulevk"\
				"imx6ulz-14x14-evk"\
				"imx6ull14x14evk"\
				"imx6ull9x9evk"\
				"imx6dlsabreauto"\
				"imx6dlsabresd"\
				"imx6qsabreauto"\
				"imx6qsabresd"\
				"imx6solosabreauto"\
				"imx6solosabresd"\
				"imx6sxsabresd"\
				"imx6sllevk"\
				"imx7dsabresd"\
				"imx7ulpevk"\
				"imx8qmmek"\
				"imx8qxpc0mek"\
				"imx8mqevk"\
				"imx8mm-lpddr4-evk"\
				"imx8mm-ddr4-evk"\
				"imx8mn-lpddr4-evk"\
				"imx8mn-ddr4-evk"\
				"imx8mp-lpddr4-evk"\
				"imx8mp-ddr4-evk"\
				"imx8dxl-lpddr4-evk"\
				"imx8mnddr3levk"\
				"imx8ulp-lpddr4-evk"\
			);;
		"imx-linux-honister")
			LIST_=(\
				"custom"\
				"imx6qpsabresd"\
				"imx6ulevk"\
				"imx6ulz-14x14-evk"\
				"imx6ull14x14evk"\
				"imx6ull9x9evk"\
				"imx6dlsabresd"\
				"imx6qsabresd"\
				"imx6solosabresd"\
				"imx6sxsabresd"\
				"imx6sllevk"\
				"imx7dsabresd"\
				"imx7ulpevk"\
				"imx8qmmek"\
				"imx8qxpc0mek"\
				"imx8mqevk"\
				"imx8mm-lpddr4-evk"\
				"imx8mm-ddr4-evk"\
				"imx8mn-lpddr4-evk"\
				"imx8mn-ddr4-evk"\
				"imx8mp-lpddr4-evk"\
				"imx8mp-ddr4-evk"\
				"imx8dxl-lpddr4-evk"\
				"imx8mnddr3levk"\
				"imx8ulp-lpddr4-evk"\
			);;
		"imx-linux-kirkstone")
			LIST_=(\
				"custom"\
				"imx6qpsabresd"\
				"imx6ulevk"\
				"imx6ulz-14x14-evk"\
				"imx6ull14x14evk"\
				"imx6ull9x9evk"\
				"imx6dlsabresd"\
				"imx6qsabresd"\
				"imx6solosabresd"\
				"imx6sxsabresd"\
				"imx6sllevk"\
				"imx7dsabresd"\
				"imx7ulpevk"\
				"imx8qmmek"\
				"imx8qxpc0mek"\
				"imx8mqevk"\
				"imx8mm-lpddr4-evk"\
				"imx8mm-ddr4-evk"\
				"imx8mn-lpddr4-evk"\
				"imx8mn-ddr4-evk"\
				"imx8mp-lpddr4-evk"\
				"imx8mp-ddr4-evk"\
				"imx8dxla1-lpddr4-evk"\
				"imx8dxlb0-lpddr4-evk"\
				"imx8dxlb0-ddr3l-evk"\
				"imx8mnddr3levk"\
				"imx8ulp-lpddr4-evk"\
				"imx8ulp-9x9-evk"\
			);;
		*)
			LIST_=(\
				"custom"\
				"imx6qpsabreauto"\
				"imx6qpsabresd"\
				"imx6ulevk"\
				"imx6ulz14x14evk"\
				"imx6ull14x14evk"\
				"imx6ull9x9evk"\
				"imx6dlsabreauto"\
				"imx6dlsabresd"\
				"imx6qsabreauto"\
				"imx6qsabresd"\
				"imx6slevk"\
				"imx6solosabreauto"\
				"imx6solosabresd"\
				"imx6sxsabresd"\
				"imx6sxsabreauto"\
				"imx6sllevk"\
				"imx7dsabresd"\
				"imx7ulpevk"\
				"imx8qmmek"\
				"imx8qxpmek"\
				"imx8qxpc0mek"\
				"imx8dxmek"\
				"imx8mqevk"\
				"imx8mmevk"\
				"imx8mnevk"\
				"imx8dxlevk"\
			);;
	esac
	set_choices ${LIST_} "MACHINE" "machine"
	unset LIST_
	MACHINE=${RETVAL}
fi

if [ -z "${IMX_IMAGE}" ]
then
	case "${TARGET_MANIFEST_BRANCH}" in
		"imx-linux-rocko")
			LIST_=(\
				"custom"\
				"core-image-minimal"\
				"core-image-base"\
				"core-image-sato"\
				"fsl-image-machine-test"\
				"fsl-image-validation-imx"\
				"fsl-image-qt5-validation-imx"\
				"fsl-image-multimedia"\
				"fsl-image-multimedia-full"\
			);;
		"imx-linux-sumo" | "imx-linux-thud")
			LIST_=(\
				"custom"\
				"core-image-minimal"\
				"core-image-base"\
				"core-image-sato"\
				"fsl-image-machine-test"\
				"fsl-image-validation-imx"\
				"fsl-image-qt5-validation-imx"\
				"fsl-image-multimedia"\
				"fsl-image-multimedia-full"\
			);;
		"imx-linux-warrior")
			LIST_=(\
				"custom"\
				"core-image-minimal"\
				"core-image-base"\
				"core-image-sato"\
				"fsl-image-machine-test"\
				"imx-image-multimedia"\
				"imx-image-full"\
			);;
		"imx-linux-zeus" |\
		"imx-linux-gatesgarth" |\
		"imx-linux-hardknott" |\
		"imx-linux-honister" |\
		"imx-linux-kirkstone")
			LIST_=(\
				"custom"\
				"core-image-minimal"\
				"core-image-base"\
				"core-image-sato"\
				"imx-image-core"\
				"fsl-image-machine-test"\
				"imx-image-multimedia"\
				"imx-image-full"\
			);;
		"custom")LIST_=("custom");;
		*)
			echo "Wrong target branch"; exit 1 ;;
	esac
	set_choices ${LIST_} "IMX_IMAGE" "imx image"
	unset LIST_
	IMX_IMAGE=${RETVAL}
fi

# backward compatiblity
if [ ! -z ${TARGET_EXTRA_META_LAYERS} ]
then
	TARGET_ADD_YOCTO_META_LAYERS="$TARGET_EXTRA_META_LAYERS"
	unset TARGET_EXTRA_META_LAYERS
fi

# backward compatiblity
if [ ! -z "${TARGET_EXTRA_YOCTO_TARGETS_CLEANALL}" ]
then
	TARGET_ADD_YOCTO_TARGETS_CLEANALL="$TARGET_EXTRA_YOCTO_TARGETS_CLEANALL"
	unset TARGET_EXTRA_YOCTO_TARGETS_CLEANALL
fi

# backward compatiblity
if [ ! -z "${TARGET_EXTRA_YOCTO_TARGETS}" ]
then
	TARGET_ADD_YOCTO_TARGETS="$TARGET_EXTRA_YOCTO_TARGETS"
	unset TARGET_EXTRA_YOCTO_TARGETS
fi

if [ ! -v TARGET_ADD_YOCTO_META_LAYERS ]
then
	TARGET_ADD_YOCTO_META_LAYERS=""
	echo "env TARGET_ADD_YOCTO_META_LAYERS is not set"
	echo -n "Add meta layers (ex. meta-example1;meta-example2;...) or leave as empty: "
	read CHOOSE_TARGET_ADD_YOCTO_META_LAYERS
	if [ ! -z "${CHOOSE_TARGET_ADD_YOCTO_META_LAYERS}" ]
	then
		IFS=';' read -ra LAYERS <<< "$CHOOSE_TARGET_ADD_YOCTO_META_LAYERS"
		for layer in "${LAYERS[@]}"
		do
			echo "Add layer '${layer}'"
		done
		unset LAYERS

		echo -n "Are added layers correct? [y/n]: "
		read -n1 CHOOSE_YES_NO
		echo ""

		if [[ "${CHOOSE_YES_NO}" =~ ^(y|Y)$ ]]
		then
			echo "Add layers into build"
		else
			echo "Reject to add layers"
			exit 1
		fi
		unset CHOOSE_YES_NO
	fi
	TARGET_ADD_YOCTO_META_LAYERS="${CHOOSE_TARGET_ADD_YOCTO_META_LAYERS}"
	unset CHOOSE_TARGET_ADD_YOCTO_META_LAYERS
fi

if [ ! -v TARGET_REMOVE_YOCTO_META_LAYERS ]
then
	TARGET_REMOVE_YOCTO_META_LAYERS=""
	echo "env TARGET_REMOVE_YOCTO_META_LAYERS is not set"
	echo -n "Remove meta layers (ex. meta-example1;meta-example2;...) or leave as empty: "
	read CHOOSE_TARGET_REMOVE_YOCTO_META_LAYERS
	if [ ! -z "${CHOOSE_TARGET_REMOVE_YOCTO_META_LAYERS}" ]
	then
		IFS=';' read -ra LAYERS <<< "$CHOOSE_TARGET_REMOVE_YOCTO_META_LAYERS"
		for layer in "${LAYERS[@]}"
		do
			echo "Remove layer '${layer}'"
		done
		unset LAYERS

		echo -n "Are removed layers correct? [y/n]: "
		read -n1 CHOOSE_YES_NO
		echo ""

		if [[ "${CHOOSE_YES_NO}" =~ ^(y|Y)$ ]]
		then
			echo "Remove layers from build"
		else
			echo "Reject to remove layers"
			exit 1
		fi
		unset CHOOSE_YES_NO
	fi
	TARGET_REMOVE_YOCTO_META_LAYERS="${CHOOSE_TARGET_REMOVE_YOCTO_META_LAYERS}"
	unset CHOOSE_TARGET_REMOVE_YOCTO_META_LAYERS
fi

if [ ! -v TARGET_ADD_YOCTO_TARGETS_CLEANSSTATE ]
then
	TARGET_ADD_YOCTO_TARGETS_CLEANSSTATE=""
	echo "env TARGET_ADD_YOCTO_TARGETS_CLEANSSTATE is not set"
	echo "NOTE: call cleansstate after fetch"
	echo -n "Add extra yocto targets that should be 'cleansstate' (ex. myapp1;myapp2;...) or leave as empty: "
	read CHOOSE_TARGET_ADD_YOCTO_TARGETS_CLEANSSTATE
	if [ ! -z "${CHOOSE_TARGET_ADD_YOCTO_TARGETS_CLEANSSTATE}" ]
	then
		IFS=';' read -ra YOCTO_TARGETS <<< "$CHOOSE_TARGET_ADD_YOCTO_TARGETS_CLEANSSTATE"
		for yocto_target in "${YOCTO_TARGETS[@]}"
		do
			echo "Add yocto target '${yocto_target}' that should be 'cleansstate'"
		done
		unset YOCTO_TARGETS

		echo -n "Are added yocto targets that should be 'cleansstate' correct? [y/n]: "
		read -n1 CHOOSE_YES_NO
		echo ""

		if [[ "${CHOOSE_YES_NO}" =~ ^(y|Y)$ ]]
		then
			echo "Add yocto targets cleansstate into build"
		else
			echo "Reject to add yocto targets cleansstate"
			exit 1
		fi
		unset CHOOSE_YES_NO
	fi
	TARGET_ADD_YOCTO_TARGETS_CLEANSSTATE="${CHOOSE_TARGET_ADD_YOCTO_TARGETS_CLEANSSTATE}"
	unset CHOOSE_TARGET_ADD_YOCTO_TARGETS_CLEANSSTATE
fi

if [ ! -v TARGET_ADD_YOCTO_TARGETS_CLEANALL ]
then
	TARGET_ADD_YOCTO_TARGETS_CLEANALL=""
	echo "env TARGET_ADD_YOCTO_TARGETS_CLEANALL is not set"
	echo "NOTE: call cleanall before fetch"
	echo -n "Add extra yocto targets that should be 'cleanall' (ex. myapp1;myapp2;...) or leave as empty: "
	read CHOOSE_TARGET_ADD_YOCTO_TARGETS_CLEANALL
	if [ ! -z "${CHOOSE_TARGET_ADD_YOCTO_TARGETS_CLEANALL}" ]
	then
		IFS=';' read -ra YOCTO_TARGETS <<< "$CHOOSE_TARGET_ADD_YOCTO_TARGETS_CLEANALL"
		for yocto_target in "${YOCTO_TARGETS[@]}"
		do
			echo "Add yocto target '${yocto_target}' that should be 'cleanall'"
		done
		unset YOCTO_TARGETS

		echo -n "Are added yocto targets that should be 'cleanall' correct? [y/n]: "
		read -n1 CHOOSE_YES_NO
		echo ""

		if [[ "${CHOOSE_YES_NO}" =~ ^(y|Y)$ ]]
		then
			echo "Add yocto targets cleanall into build"
		else
			echo "Reject to add yocto targets cleanall"
			exit 1
		fi
		unset CHOOSE_YES_NO
	fi
	TARGET_ADD_YOCTO_TARGETS_CLEANALL="${CHOOSE_TARGET_ADD_YOCTO_TARGETS_CLEANALL}"
	unset CHOOSE_TARGET_ADD_YOCTO_TARGETS_CLEANALL
fi

if [ ! -v TARGET_ADD_YOCTO_TARGETS ]
then
	TARGET_ADD_YOCTO_TARGETS=""
	echo "env TARGET_ADD_YOCTO_TARGETS is not set"
	echo -n "Add yocto targets (ex. myapp1;myapp2;...) or leave as empty: "
	read CHOOSE_TARGET_ADD_YOCTO_TARGETS
	if [ ! -z "${CHOOSE_TARGET_ADD_YOCTO_TARGETS}" ]
	then
		IFS=';' read -ra YOCTO_TARGETS <<< "$CHOOSE_TARGET_ADD_YOCTO_TARGETS"
		for yocto_target in "${YOCTO_TARGETS[@]}"
		do
			echo "Add yocto target '${yocto_target}'"
		done
		unset YOCTO_TARGETS

		echo -n "Are added yocto targets correct? [y/n]: "
		read -n1 CHOOSE_YES_NO
		echo ""

		if [[ "${CHOOSE_YES_NO}" =~ ^(y|Y)$ ]]
		then
			echo "Add yocto targets into build"
		else
			echo "Reject to add yocto targets"
			exit 1
		fi
		unset CHOOSE_YES_NO
	fi
	TARGET_ADD_YOCTO_TARGETS="${CHOOSE_TARGET_ADD_YOCTO_TARGETS}"
	unset CHOOSE_TARGET_ADD_YOCTO_TARGETS
fi

if [ -z ${TARGET_ADD_YOCTO_TARGETS} ]
then
	TARGET_ADD_YOCTO_TARGETS="${IMX_IMAGE}"
else
	TARGET_ADD_YOCTO_TARGETS="${TARGET_ADD_YOCTO_TARGETS};${IMX_IMAGE}"
fi

# reject backward compatiblity
if [ ! -z "$(printenv | grep '^LOCAL_CONF_ENV_')" ]
then
	echo "LOCAL_CONF_ENV_ env is deprecated, please use TARGET_YOCTO_EXTRA_LOCAL_CONF instead"
	echo "Example, save extra local.conf variables into file"
	echo "cat <<EOF > extra_local.conf"
	echo "MY_ENV = \"value\""
	echo "EOF"
	echo "And export env TARGET_YOCTO_EXTRA_LOCAL_CONF=extra_local.conf"
fi
# Pass extra env vars for local.conf
if [ -e "$TARGET_YOCTO_EXTRA_LOCAL_CONF" ]
then
    TARGET_YOCTO_EXTRA_LOCAL_CONF="--mount type=bind,source=$TARGET_YOCTO_EXTRA_LOCAL_CONF,target=/home/build/extra_local.conf"
else
    unset TARGET_YOCTO_EXTRA_LOCAL_CONF
fi

# Pass icecc.conf
if [ -e "$TARGET_YOCTO_ICECC_CONF" ]
then
    TARGET_YOCTO_ICECC_CONF="--mount type=bind,source=$TARGET_YOCTO_ICECC_CONF,target=/etc/icecc/icecc.conf"
    TARGET_YOCTO_ICECCD_RUN="yes"
else
    unset TARGET_YOCTO_ICECC_CONF
    unset TARGET_YOCTO_ICECCD_RUN
fi

# Set uid and gid to match host current user as long as NOT root
if [ $(id -u) -ne "0" ]
then
	HOST_ID_ARGS="--env USER_ID=$(id -u) --env GROUP_ID=$(id -g)"
else
	echo "Compilation under 'root' not supported"
	exit 1
fi

# MacOS does not support ssh socket
if [ "$(uname)" == "Darwin" ]
then
	# MacOS requires privileged
	DARWIN_ARGS="--privileged"
	if [ -d "$SSH_DIR" ]
	then
		SSH_ARGS="--volume ${SSH_DIR}:/home/build/.ssh"
	else
		echo "[WARNING] SSH_DIR is not set"
	fi
else
	if [ ! -S "$SSH_AUTH_SOCK" ]
	then
		# Kill ssh-agent
		trap "ssh-agent -k" exit
		eval "$(ssh-agent -s)"
		if [ -n "$SSH_PRIVATE_KEY_PATH" ]
		then
			if [ -f "$SSH_PRIVATE_KEY_PATH" ]
			then
				ssh-add "$SSH_PRIVATE_KEY_PATH"
			else
				echo "[WARNING] SSH_PRIVATE_KEY_PATH: '$SSH_PRIVATE_KEY_PATH' not found"
				exit 1
			fi
		else
			# add default keys
			if [ -f ~/.ssh/id_rsa ]
			then
				ssh-add ~/.ssh/id_rsa
			fi
			if [ -f ~/.ssh/id_ed25519 ]
			then
				ssh-add ~/.ssh/id_ed25519
			fi
		fi
	fi

	if [ -S "$SSH_AUTH_SOCK" ]
	then
		SSH_ARGS="--volume ${SSH_AUTH_SOCK}:/ssh-agent --env SSH_AUTH_SOCK=/ssh-agent"
	else
		echo "[WARNING] SSH_AUTH_SOCK is not set"
	fi
fi

echo "Starting Docker..."

if [ -z "$DOCKER_COMMAND" ]
then
	DOCKER_TTY="--interactive --tty"
fi

if [ -n "$DOCKER_NAME" ]
then
	DOCKER_NAME="--name=${DOCKER_NAME}"
fi

docker run \
	--rm \
	${DOCKER_EXTRA_FLAGS} \
	${DOCKER_TTY} \
	${DOCKER_NAME} \
	${HOST_ID_ARGS} \
	${SSH_ARGS} \
	${NICE_PRIO_ARGS} \
	${DARWIN_ARGS} \
	--env BUILD_PRIO="${BUILD_PRIO}" \
	--env MACHINE="${MACHINE}" \
	--env DISTRO="${DISTRO}" \
	--env TARGET_ADD_YOCTO_META_LAYERS="${TARGET_ADD_YOCTO_META_LAYERS}" \
	--env TARGET_REMOVE_YOCTO_META_LAYERS="${TARGET_REMOVE_YOCTO_META_LAYERS}" \
	--env TARGET_ADD_YOCTO_TARGETS="${TARGET_ADD_YOCTO_TARGETS}" \
	--env TARGET_ADD_YOCTO_TARGETS_CLEANSSTATE="${TARGET_ADD_YOCTO_TARGETS_CLEANSSTATE}" \
	--env TARGET_ADD_YOCTO_TARGETS_CLEANALL="${TARGET_ADD_YOCTO_TARGETS_CLEANALL}" \
	--env TARGET_CUSTOM_BUILD_DIR="${TARGET_CUSTOM_BUILD_DIR}" \
	--env TARGET_MANIFEST_URL="${TARGET_MANIFEST_URL}" \
	--env TARGET_MANIFEST_BRANCH="${TARGET_MANIFEST_BRANCH}" \
	--env TARGET_MANIFEST_NAME="${TARGET_MANIFEST_NAME}" \
	--env TARGET_REPO_SYNC_LOCAL="${TARGET_REPO_SYNC_LOCAL}" \
	${TARGET_YOCTO_EXTRA_LOCAL_CONF} \
	${TARGET_YOCTO_ICECC_CONF} \
	--env TARGET_YOCTO_ICECCD_RUN="${TARGET_YOCTO_ICECCD_RUN}" \
	--volume="${WORKDIR}":/home/build/yocto \
	${DOCKER_IMAGE} \
	${DOCKER_COMMAND}
