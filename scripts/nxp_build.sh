#!/bin/bash

CPU_NUM=$(nproc)

set -e

# exit from script and kill all background jobs
function clean_on_exit()
{
    RC=$?
    echo "$0: exit code $RC"
    set +e
    echo "$0: killing background jobs..."
    if [ ! -z "$(jobs -p)" ]
    then
        kill $(jobs -p)
        sleep 1
    fi
    while [ ! -z "$(jobs -p)" ]
    do
        echo "$0: force killing background jobs..."
        kill -SIGKILL $(jobs -p)
        sleep 1
    done
    echo "$0: all background jobs have been stopped"
    echo $RC
}
readonly -f clean_on_exit
trap clean_on_exit HUP INT ABRT ERR TERM EXIT

function do_add_yocto_targets()
{
    if [ ! -z "${TARGET_ADD_YOCTO_TARGETS}" ]
    then
        IFS=';' read -ra YOCTO_TARGETS <<< "$TARGET_ADD_YOCTO_TARGETS"
        for yocto_target in "${YOCTO_TARGETS[@]}"
        do
            ${BUILD_PRIO}\
            bitbake ${yocto_target} $@
        done
        unset YOCTO_TARGETS
    fi
}

function do_add_yocto_targets_cleansstate()
{
    if [ ! -z "${TARGET_ADD_YOCTO_TARGETS_CLEANSSTATE}" ]
    then
        IFS=';' read -ra YOCTO_TARGETS <<< "$TARGET_ADD_YOCTO_TARGETS_CLEANSSTATE"
        for (( idx=${#YOCTO_TARGETS[@]}-1 ; idx>=0 ; idx-- ))
        do
            ${BUILD_PRIO}\
            bitbake "${YOCTO_TARGETS[idx]}" -c cleansstate
        done
        unset YOCTO_TARGETS
    fi
}

function do_add_yocto_targets_cleanall()
{
    if [ ! -z "${TARGET_ADD_YOCTO_TARGETS_CLEANALL}" ]
    then
        IFS=';' read -ra YOCTO_TARGETS <<< "$TARGET_ADD_YOCTO_TARGETS_CLEANALL"
        for (( idx=${#YOCTO_TARGETS[@]}-1 ; idx>=0 ; idx-- ))
        do
            ${BUILD_PRIO}\
            bitbake "${YOCTO_TARGETS[idx]}" -c cleanall
        done
        unset YOCTO_TARGETS
    fi
}

function do_inject_local_conf_envs()
{
    sed -i '/#LOCAL_CONF_ENV/,/#LOCAL_CONF_ENV/d' conf/local.conf
    echo '#LOCAL_CONF_ENV' >> conf/local.conf
    cat ~/extra_local.conf >> conf/local.conf
    echo '#LOCAL_CONF_ENV' >> conf/local.conf
}

function restart_iceccd()
{
    while true; do
        # Check if iceccd is running
        if ! pgrep -x "iceccd" > /dev/null; then
            sudo /etc/init.d/iceccd restart
        fi
        sleep 10
    done
}

if [ ! -z "${TARGET_YOCTO_ICECCD_RUN}" ]
then
    restart_iceccd &
fi

cd ~/yocto

mkdir -p nxp-bsp-platform && cd nxp-bsp-platform

echo "Initialize manifest of repositories..."
${BUILD_PRIO}\
repo init \
    --submodules \
    --manifest-url=${TARGET_MANIFEST_URL} \
    --manifest-branch=${TARGET_MANIFEST_BRANCH} \
    --manifest-name="${TARGET_MANIFEST_NAME}.xml" \
    --quiet
echo "Manifest is successful initialized"

echo "Fetching repositories..."
${BUILD_PRIO}\
repo sync \
    --force-sync \
    --force-remove-dirty \
    --prune \
    --network-only \
    --jobs=${CPU_NUM} \
    --current-branch \
    --no-tags \
    --quiet
echo "Repositories are successful fetched"

echo "Synchronizing repositories..."
${BUILD_PRIO}\
repo sync \
    --force-sync \
    --force-remove-dirty \
    --detach \
    --local-only \
    --jobs=${CPU_NUM} \
    --current-branch \
    --no-tags \
    --quiet \
    ${TARGET_REPO_SYNC_LOCAL}
echo "Repositories are successful synchronized"

# env MACHINE already exported
# env DISTRO already exported

if [ ! -z "$TARGET_CUSTOM_BUILD_DIR" ]
then
    BUILD_DIR="$TARGET_CUSTOM_BUILD_DIR"
else
    BUILD_DIR="build-${MACHINE}-${DISTRO}"
fi

if [ -e imx-setup-release.sh ]
then
    EULA=1 SDKMACHINE=x86_64 source imx-setup-release.sh -b "$BUILD_DIR"
elif [ -e fsl-setup-release.sh ]
then
    EULA=1 SDKMACHINE=x86_64 source fsl-setup-release.sh -b "$BUILD_DIR"
else
    EULA=1 SDKMACHINE=x86_64 source setup-environment "$BUILD_DIR"
fi

do_inject_local_conf_envs

BITBAKE_VERSION=$(bitbake --version)
BITBAKE_VERSION=${BITBAKE_VERSION##* }

# Yocto Sumo 2.5 (bitbake 1.38) changes for fetch all sources
if [ "$(echo -e ''${BITBAKE_VERSION}\\n1.38'' | sort -V | head -n1)" == "1.38" ]
then
    FETCH_ALL_ARG="--runall=fetch"
else
    FETCH_ALL_ARG="-c fetchall"
fi

unset BITBAKE_VERSION

if [ ! -z "${TARGET_REMOVE_YOCTO_META_LAYERS}" ]
then
    IFS=';' read -ra LAYERS <<< "$TARGET_REMOVE_YOCTO_META_LAYERS"
    for layer in "${LAYERS[@]}"
    do
        ${BUILD_PRIO}\
        bitbake-layers remove-layer ${layer} || true
    done
    unset LAYERS
fi

if [ ! -z "${TARGET_ADD_YOCTO_META_LAYERS}" ]
then
    IFS=';' read -ra LAYERS <<< "$TARGET_ADD_YOCTO_META_LAYERS"
    for layer in "${LAYERS[@]}"
    do
        ${BUILD_PRIO}\
        bitbake-layers add-layer ../sources/${layer}
    done
    unset LAYERS
fi

#############
### CLEAN ###
#############
# clean all added Yocto targets
do_add_yocto_targets_cleanall

#############
### FETCH ###
#############
# fetch all required sources
${BUILD_PRIO}\
bitbake u-boot ${FETCH_ALL_ARG}
${BUILD_PRIO}\
bitbake virtual/kernel ${FETCH_ALL_ARG}

# fetch all added Yocto targets
do_add_yocto_targets ${FETCH_ALL_ARG}

####################
### CLEAN SSTATE ###
####################
# clean sstate all added Yocto targets
do_add_yocto_targets_cleansstate

#############
### BUILD ###
#############
${BUILD_PRIO}\
bitbake u-boot

# build all added Yocto targets
do_add_yocto_targets

${BUILD_PRIO}\
bitbake package-index
